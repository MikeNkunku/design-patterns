/**
 * Package containing common models and interfaces that can be used for testing all design patterns.
 */
package com.nkunku.models;
