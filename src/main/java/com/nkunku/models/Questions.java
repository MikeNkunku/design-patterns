package com.nkunku.models;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import com.nkunku.models.impl.AbstractHuman;
import com.nkunku.models.impl.AbstractHuman.Builder;

/**
 * Questions to be asked to the user in order to fill the various information regarding a {@link AbstractHuman human}.
 *
 * @author Mike
 */
public enum Questions {

	/** Question for the gender. */
	GENDER("What is the gender? (F or M)",null),
	/** Question regarding the age. */
	AGE("How old do you want the human to be?", (builder, age) -> builder.withAge(Integer.parseInt(age))),
	/** Question about the first name. */
	FIRSTNAME("What's the human's first name?", Builder::withFirstName),
	/** Question about the last name. */
	LASTNAME("What's the human's last name?", Builder::withLastName),
	/** Question regarding the height. */
	HEIGHT("How big do you want the person to be? (meters)", (builder, height) -> builder.withHeight(Double.parseDouble(height))),
	/** Question regarding the height. */
	WEIGHT("How much should the person weigh? (kgs)", (builder, weight) -> builder.withWeight(Double.parseDouble(weight)));

	/** Question to ask to the user. */
	private final String question;
	/** {@link BiFunction} which updates the provided builder with the answer given by the user after parsing. */
	private final BiConsumer<Builder<?, ?>, String> builderUpdater;

	/**
	 * @param question The question to ask the user.
	 * @param builderUpdater The method to call in order to update the provided builder.
	 */
	Questions(final String question, final BiConsumer<Builder<?, ?>, String> builderUpdater) {
		this.question = question;
		this.builderUpdater = builderUpdater;
	}

	/**
	 * @return The question to ask the user.
	 */
	public String question() {
		return question;
	}

	/**
	 * @return The method to call in order to update the provided builder.
	 */
	public BiConsumer<Builder<?, ?>, String> builderUpdater() {
		return builderUpdater;
	}
}
