package com.nkunku.models.impl;

/**
 * @author Mike
 */
public final class Male extends AbstractHuman {

	/**
	 * Default constructor which cannot be called: use the {@link Builder builder} instead.
	 */
	private Male() {
		super();
	}

	/**
	 * Builder to create a {@link Male male} instance.
	 */
	public static class Builder extends AbstractHuman.Builder<Male, Builder> {

		@Override
		protected Male newInstance() {
			final Male male = new Male();
			male.setGender(Gender.MALE);
			return male;
		}

		@Override
		protected Builder self() {
			return this;
		}
	}
}
