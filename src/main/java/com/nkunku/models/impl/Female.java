package com.nkunku.models.impl;

/**
 * @author Mike
 */
public final class Female extends AbstractHuman {

	/**
	 * Default constructor which cannot be called: use the {@link Builder builder} instead.
	 */
	private Female() {
		super();
	}

	/**
	 * Builder to create a {@link Female female} instance.
	 */
	public static class Builder extends AbstractHuman.Builder<Female, Builder> {

		@Override
		protected Female newInstance() {
			final Female female = new Female();
			female.setGender(Gender.FEMALE);
			return female;
		}

		@Override
		protected Builder self() {
			return this;
		}
	}
}
