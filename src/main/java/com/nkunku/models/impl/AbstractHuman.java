package com.nkunku.models.impl;

import com.nkunku.models.HumanBeing;

/**
 * Classes holding common attributes that can be held by a human.
 *
 * @author Mike
 */
public abstract class AbstractHuman implements HumanBeing {

	/** The gender. */
	private Gender gender;
	/** The first name. */
	private String firstName;
	/** The last name. */
	private String lastName;
	/** The age. */
	private int age;
	/** The height. */
	private double height;
	/** The weight. */
	private double weight;


	@Override
	public Gender getGender() {
		return gender;
	}

	/**
	 * @param gender The gender.
	 */
	void setGender(final Gender gender) {
		this.gender = gender;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName The first name.
	 */
	void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName The last name.
	 */
	void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	@Override
	public int getAge() {
		return age;
	}

	/**
	 * @param age The age.
	 */
	void setAge(final int age) {
		this.age = age;
	}

	@Override
	public double getHeight() {
		return height;
	}

	/**
	 * @param height The height.
	 */
	void setHeight(final double height) {
		this.height = height;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	/**
	 * @param weight The weight.
	 */
	void setWeight(final double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "Human [Gender=" + gender.name()
			+ ", fullName=" + firstName + ' ' + lastName
			+ ", age=" + age
			+ ", height=" + height + 'm'
			+ ", weight=" + weight + "kg"
			+ ']';
	}


	/**
	 * Builder which facilitate the creation of humans.
	 *
	 * @param <HUMAN> The concrete type of human.
	 * @param <BUILDER> The concrete type of builder.
	 */
	public static abstract class Builder<HUMAN extends AbstractHuman, BUILDER extends Builder<HUMAN, BUILDER>> {
		/**
		 * @implSpec The gender must be set in this method.
		 * @return A new instance of the concrete type of human.
		 */
		protected abstract HUMAN newInstance();
		/** @return The current builder; */
		protected abstract BUILDER self();

		/** The underlying instance. */
		private HUMAN instance;

		/**
		 * Default constructor.
		 */
		Builder() {
			super();
			instance = newInstance();
		}

		/**
		 * @param firstName The first name.
		 * @return The current builder.
		 */
		public BUILDER withFirstName(final String firstName) {
			instance.setFirstName(firstName);
			return self();
		}

		/**
		 * @param lastName The last name.
		 * @return The current builder.
		 */
		public BUILDER withLastName(final String lastName) {
			instance.setLastName(lastName);
			return self();
		}

		/**
		 * @param age The age.
		 * @return The current builder.
		 */
		public BUILDER withAge(final int age) {
			instance.setAge(age);
			return self();
		}

		/**
		 * @param height The height.
		 * @return The current builder.
		 */
		public BUILDER withHeight(final double height) {
			instance.setHeight(height);
			return self();
		}

		/**
		 * @param weight The weight.
		 * @return The current builder.
		 */
		public BUILDER withWeight(final double weight) {
			instance.setWeight(weight);
			return self();
		}

		/**
		 * @return The underlying instance enriched.
		 */
		public HUMAN build() {
			final HUMAN human = instance;
			instance = newInstance();
			return human;
		}
	}
}
