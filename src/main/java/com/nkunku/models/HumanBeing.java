package com.nkunku.models;

/**
 * Interface describing some properties that a human being can be specified.
 *
 * @author Mike
 */
public interface HumanBeing {

	/** @return The age. */
	int getAge();
	/** @return The gender. */
	Gender getGender();
	/** @return The first name. */
	String getFirstName();
	/** @return The last name. */
	String getLastName();
	/** @return The height. */
	double getHeight();
	/** @return The weight. */
	double getWeight();

	/**
	 * Enumeration holding the gender of {@link HumanBeing human being}.
	 */
	enum Gender {
		/** Male. */
		MALE("M"),
		/** Female. */
		FEMALE("F");

		/** Identifier. */
		private final String identifier;


		/**
		 * @param identifier The identifier.
		 */
		Gender(final String identifier) {
			this.identifier = identifier;
		}


		/**
		 * @return The identifier.
		 */
		public String identifier() {
			return identifier;
		}


		/**
		 * @param identifier The identifier for the gender.
		 * @return The found gender.
		 * @throws IllegalArgumentException When no gender could be found.
		 */
		public static Gender from(final String identifier) {
			for (final Gender gender : values()) {
				if (gender.identifier.equals(identifier)) {
					return gender;
				}
			}
			throw new IllegalArgumentException("No gender could be found for the identifier '" + identifier + '"');
		}
	}
}
