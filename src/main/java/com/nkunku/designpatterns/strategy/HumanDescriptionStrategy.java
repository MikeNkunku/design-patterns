package com.nkunku.designpatterns.strategy;

import com.nkunku.models.HumanBeing;

/**
 * Strategy to describe a human being.
 *
 * @author Mike
 */
@FunctionalInterface
public interface HumanDescriptionStrategy {

	/**
	 * @param humanBeing The human being to describe.
	 */
	void describe(HumanBeing humanBeing);
}
