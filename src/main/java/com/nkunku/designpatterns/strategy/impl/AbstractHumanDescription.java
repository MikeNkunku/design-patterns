package com.nkunku.designpatterns.strategy.impl;

import java.io.PrintStream;

import com.nkunku.designpatterns.strategy.HumanDescriptionStrategy;
import com.nkunku.models.HumanBeing;
import com.nkunku.models.HumanBeing.Gender;

/**
 * Common code of human description.
 *
 * @author Mike
 */
abstract class AbstractHumanDescription implements HumanDescriptionStrategy {

	@Override
	public void describe(final HumanBeing humanBeing) {
		final String personalPronoun = getPersonalPronoun();
		final StringBuilder stringBuilder = new StringBuilder(String.format(
				"%n%s %s is a %s.", humanBeing.getFirstName(), humanBeing.getLastName(), getGender(humanBeing)))
				.append(String.format("%n%s's %d.", personalPronoun, Integer.valueOf(humanBeing.getAge())))
				.append(String.format("%n%s's %.2f m tall.", personalPronoun, Double.valueOf(humanBeing.getHeight())))
				.append(String.format("%n%s's %.1f kg.", personalPronoun, Double.valueOf(humanBeing.getWeight())));

		try (final PrintStream console = System.out) {
			console.println(stringBuilder.toString());
		}
	}

	/** @return The right personal pronoun. */
	protected abstract String getPersonalPronoun();


	/**
	 * @param humanBeing The human being to describe.
	 * @return An understandable description of the human being gender.
	 */
	private static String getGender(final HumanBeing humanBeing) {
		return Gender.FEMALE.equals(humanBeing.getGender()) ? "female" : "male";
	}
}
