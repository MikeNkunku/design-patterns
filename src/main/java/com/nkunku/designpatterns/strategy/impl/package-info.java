/**
 * Package dedicated to the implementations of interfaces for the demonstration of the <b>Strategy</b> design pattern.
 */
package com.nkunku.designpatterns.strategy.impl;
