package com.nkunku.designpatterns.strategy.impl;

/**
 * @author Mike
 */
public class MaleDescription extends AbstractHumanDescription {

	@Override
	protected String getPersonalPronoun() {
		return "He";
	}
}
