package com.nkunku.designpatterns.strategy.impl;

/**
 * @author Mike
 */
public class FemaleDescription extends AbstractHumanDescription {

	@Override
	protected String getPersonalPronoun() {
		return "She";
	}
}
