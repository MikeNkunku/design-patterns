package com.nkunku.designpatterns.strategy;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.nkunku.designpatterns.strategy.impl.FemaleDescription;
import com.nkunku.designpatterns.strategy.impl.MaleDescription;
import com.nkunku.models.HumanBeing.Gender;
import com.nkunku.models.Questions;
import com.nkunku.models.impl.AbstractHuman;
import com.nkunku.models.impl.Female.Builder;
import com.nkunku.models.impl.Male;

/**
 * @author Mike
 */
public final class Demo {

	/**
	 * @param args Arguments provided via command line.
	 */
	public static void main(final String[] args) {
		AbstractHuman.Builder<? extends AbstractHuman, ? extends AbstractHuman.Builder<?, ?>> builder = null;
		HumanDescriptionStrategy strategy = null;
		boolean validAnswer = false;
		System.out.println(Questions.GENDER.question());

		try (final Scanner scanner = new Scanner(System.in)) {
			while (!validAnswer) {
				try {
					final String genderIdentifier = scanner.nextLine();
					builder = Gender.FEMALE.equals(Gender.from(genderIdentifier)) ? new Builder() : new Male.Builder();
					strategy = Gender.FEMALE.equals(Gender.from(genderIdentifier)) ? new FemaleDescription() : new MaleDescription();
					validAnswer = true;
				} catch (final IllegalArgumentException e) {
					System.out.println(e.getMessage());
				}
			}

			final List<Questions> remainingQuestions = Arrays.stream(Questions.values()).filter(question -> !Questions.GENDER.equals(question)).collect(Collectors.toList());
			for (final Questions question : remainingQuestions) {
				handleQuestion(scanner, question, builder);
			}
		}

		strategy.describe(builder.build());
	}


	/**
	 * @param scanner The scanner to get the user answer.
	 * @param question The question to ask to the user.
	 * @param builder The builder to update.
	 */
	private static void handleQuestion(final Scanner scanner, final Questions question,
			final AbstractHuman.Builder<? extends AbstractHuman, ? extends AbstractHuman.Builder<?, ?>> builder) {
		System.out.println(question.question());
		question.builderUpdater().accept(builder, scanner.nextLine());
	}
}
