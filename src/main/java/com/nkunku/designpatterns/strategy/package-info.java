/**
 * Package dedicated to the demonstration of the <b>Strategy</b> design pattern.
 */
package com.nkunku.designpatterns.strategy;
