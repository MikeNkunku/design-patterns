package com.nkunku.designpatterns.observer;

import java.io.Serializable;

/**
 * @author Mike
 */
public interface Observer {
	/**
	 * @param event The event to process.
	 */
	void update(Event<Serializable> event);
	/**
	 * @return The stateful string representation of the current instance.
	 */
	String displayStatus();
}
