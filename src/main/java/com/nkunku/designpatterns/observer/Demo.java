package com.nkunku.designpatterns.observer;

import java.io.Serializable;
import java.math.BigInteger;

import com.nkunku.designpatterns.observer.impl.DefaultEvent;
import com.nkunku.designpatterns.observer.impl.DefaultObserver;
import com.nkunku.designpatterns.observer.impl.DefaultSubject;

/**
 * @author Mike
 */
public class Demo {

	/**
	 * @param args The arguments provided by command line.
	 */
	public static void main(final String[] args) {
		final Observer observer1 = new DefaultObserver();
		final Observer observer2 = new DefaultObserver();
		final Subject<?> subject1  = new DefaultSubject().withObserver(observer1);
		final Subject<?> subject2 = new DefaultSubject().withObserver(observer2);

		final Event<Serializable> event1 = new DefaultEvent<>(Integer.valueOf(1), "That's that s**t...");
		final Event<Serializable> event2 = new DefaultEvent<>(Integer.valueOf(2), BigInteger.valueOf(120_000L));

		subject1.fireEvent(event1);
		subject2.fireEvent(event1);

		System.out.format("Observer#1 : %s%nObserver#2: %s%n", observer1.displayStatus(), observer2.displayStatus());

		subject1.fireEvent(event2);
		subject2.fireEvent(event2);

		System.out.format("%nObserver#1 : %s%nObserver#2: %s%n", observer1.displayStatus(), observer2.displayStatus());
	}
}
