package com.nkunku.designpatterns.observer.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.WeakHashMap;

import com.nkunku.designpatterns.observer.Event;
import com.nkunku.designpatterns.observer.Observer;
import com.nkunku.designpatterns.observer.Subject;

/**
 * Class holding the common behaviour and attributes for implementations of {@link Subject}.
 *
 * @author Mike
 */
abstract class AbstractSubject<SUBJECT extends Subject<SUBJECT>> implements Subject<SUBJECT> {

	/** The collection of observers. */
	private final WeakHashMap<String, Observer> observers;


	/**
	 * Default constructor.
	 */
	AbstractSubject() {
		this(new WeakHashMap<>());
	}

	/**
	 * @param observers The initial list of observers.
	 */
	AbstractSubject(final WeakHashMap<String, Observer> observers) {
		super();
		this.observers = Objects.requireNonNull(observers, "The map of observers cannot be null");
	}


	@Override
	public SUBJECT withObserver(final Observer observer) {
		observers.put(observer.toString(), observer);
		return self();
	}

	@Override
	public SUBJECT withObservers(final Collection<? extends Observer> observers) {
		observers.forEach(this::withObserver);
		return self();
	}

	@Override
	public void fireEvent(final Event<Serializable> event) {
		observers.forEach((id, observer) -> observer.update(event));
	}

	/**
	 * @return The current instance.
	 */
	protected abstract SUBJECT self();
}
