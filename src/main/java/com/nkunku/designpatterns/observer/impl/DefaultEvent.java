package com.nkunku.designpatterns.observer.impl;

import java.io.Serializable;

/**
 * Default implementation of {@link com.nkunku.designpatterns.observer.Event}.
 *
 * @param <CONTENT> The concrete type of the content.
 *
 * @author Mike
 */
public class DefaultEvent<CONTENT extends Serializable> extends AbstractEvent<CONTENT> {

	/**
	 * @param sourceId     The source ID.
	 * @param serializable The content.
	 */
	public DefaultEvent(final Integer sourceId, final CONTENT serializable) {
		super(sourceId, serializable);
	}
}
