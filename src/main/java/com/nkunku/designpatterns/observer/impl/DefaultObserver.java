package com.nkunku.designpatterns.observer.impl;

/**
 * Default implementation of {@link com.nkunku.designpatterns.observer.Observer Observer}.
 *
 * @author Mike
 */
public class DefaultObserver extends AbstractObserver {}
