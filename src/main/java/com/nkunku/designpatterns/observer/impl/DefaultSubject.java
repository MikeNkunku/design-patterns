package com.nkunku.designpatterns.observer.impl;

import java.util.WeakHashMap;

import com.nkunku.designpatterns.observer.Observer;

/**
 * Default implementation of {@link com.nkunku.designpatterns.observer.Subject Subject}.
 *
 * @author Mike
 */
public class DefaultSubject extends AbstractSubject<DefaultSubject> {

	/**
	 * Default constructor.
	 */
	public DefaultSubject() {
		super();
	}

	/**
	 * @param observers The initial collection of observers.
	 */
	public DefaultSubject(final WeakHashMap<String, Observer> observers) {
		super(observers);
	}

	@Override
	protected DefaultSubject self() {
		return this;
	}
}
