package com.nkunku.designpatterns.observer.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.nkunku.designpatterns.observer.Event;
import com.nkunku.designpatterns.observer.Observer;

/**
 * Class holding common attributes for {@link Observer} implementations.
 *
 * @author Mike
 */
abstract class AbstractObserver implements Observer {

	/** The map containing data regarding the subjects. */
	private final Map<Integer, Serializable> subjectsMap;


	/**
	 * Default constructor.
	 */
	AbstractObserver() {
		this(new HashMap<>());
	}

	/**
	 * @param subjectsMap An empty map to contain data regarding the subjects.
	 */
	AbstractObserver(final Map<Integer, Serializable> subjectsMap) {
		super();
		this.subjectsMap = Objects.requireNonNull(subjectsMap, "The map tracking the subjects cannot be null");
	}


	@Override
	public void update(final Event<Serializable> event) {
		subjectsMap.put(event.getSourceId(), event.getContent());
	}

	@Override
	public String displayStatus() {
		final StringBuilder stringBuilder = new StringBuilder("[\n");
		subjectsMap.forEach((id, content) -> stringBuilder.append("    ").append(id).append(" -> ").append(content).append('\n'));
		return stringBuilder.append(']').toString();
	}
}
