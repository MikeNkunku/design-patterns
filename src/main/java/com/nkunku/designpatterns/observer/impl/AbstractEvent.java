package com.nkunku.designpatterns.observer.impl;

import java.io.Serializable;
import java.util.Objects;

import com.nkunku.designpatterns.observer.Event;

/**
 * Class holding the common behaviour of {@link Event} implementations.
 *
 * @author Mike
 */
abstract class AbstractEvent<CONTENT extends Serializable> implements Event<CONTENT> {

	/** The source ID. */
	private final Integer sourceId;
	/** The content. */
	private final CONTENT content;


	/**
	 * @param sourceId The source ID.
	 * @param content The content.
	 */
	AbstractEvent(final Integer sourceId, final CONTENT content) {
		super();
		this.sourceId = Objects.requireNonNull(sourceId, "The source identifier cannot be null");
		this.content = Objects.requireNonNull(content, "The content cannot be blank");
	}


	@Override
	public Integer getSourceId() {
		return sourceId;
	}

	@Override
	public CONTENT getContent() {
		return content;
	}
}
