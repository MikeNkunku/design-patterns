/**
 * Package containing the different implementations of interfaces to illustrate the beauty of the <b>Observer</b> design pattern.
 */
package com.nkunku.designpatterns.observer.impl;
