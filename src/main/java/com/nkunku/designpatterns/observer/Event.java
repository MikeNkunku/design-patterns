package com.nkunku.designpatterns.observer;

import java.io.Serializable;

/**
 * Interface describing the basic contract of an event.
 *
 * @param <CONTENT> The concrete tye of content held by the event.
 *
 * @author Mike
 */
public interface Event<CONTENT extends Serializable> {

	/** @return The source identifier. */
	Integer getSourceId();
	/** @return The content. */
	CONTENT getContent();
}
