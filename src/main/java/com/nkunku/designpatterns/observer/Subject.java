package com.nkunku.designpatterns.observer;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface describing a subject which can be the target of {@link Observer observers}.
 *
 * @param <SUBJECT> The concrete type of implementation.
 *
 * @author Mike
 */
public interface Subject<SUBJECT extends Subject<SUBJECT>> {

	/**
	 * @param event The event to fire.
	 */
	void fireEvent(Event<Serializable> event);

	/**
	 * @param observer The observer to add.
	 */
	SUBJECT withObserver(Observer observer);

	/**
	 * @param observers The multiple observers to add.
	 */
	SUBJECT withObservers(final Collection<? extends Observer> observers);
}
