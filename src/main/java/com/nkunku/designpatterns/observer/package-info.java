/**
 * Package containing interfaces and the class to run the demonstration of the <b>Observer</b> design pattern.
 */
package com.nkunku.designpatterns.observer;
