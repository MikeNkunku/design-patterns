package com.nkunku.designpatterns.factory;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.nkunku.designpatterns.strategy.HumanDescriptionStrategy;
import com.nkunku.models.HumanBeing.Gender;
import com.nkunku.models.Questions;
import com.nkunku.models.impl.AbstractHuman;

/**
 * @author Mike
 */
public final class Demo {

	/**
	 * @param args Arguments provided via command line.
	 */
	public static void main(final String[] args) {
		boolean validAnswer = false;
		System.out.println(Questions.GENDER.question());

		AbstractHuman.Builder<? extends AbstractHuman, ? extends AbstractHuman.Builder<?, ?>> builder;
		Gender identifier = null;
		try (final Scanner scanner = new Scanner(System.in)) {
			while (!validAnswer) {
				try {
					identifier = Gender.from(scanner.nextLine());
					validAnswer = true;
				} catch (final IllegalArgumentException e) {
					System.out.println(e.getMessage());
				}
			}

			builder = HumanBuilderFactory.fromGender(identifier);
			final List<Questions> remainingQuestions = Arrays.stream(Questions.values()).filter(question -> !Questions.GENDER.equals(question)).collect(Collectors.toList());
			for (final Questions question : remainingQuestions) {
				handleQuestion(scanner, question, builder);
			}
		}

		final HumanDescriptionStrategy strategy = HumanDescriptionFactory.fromGender(identifier);
		strategy.describe(builder.build());
	}


	/**
	 * @param scanner The scanner to get the user answer.
	 * @param question The question to ask to the user.
	 * @param builder The builder to update.
	 */
	private static void handleQuestion(final Scanner scanner, final Questions question,
			final AbstractHuman.Builder<? extends AbstractHuman, ? extends AbstractHuman.Builder<?, ?>> builder) {
		System.out.println(question.question());
		question.builderUpdater().accept(builder, scanner.nextLine());
	}
}
