package com.nkunku.designpatterns.factory;

import com.nkunku.designpatterns.strategy.HumanDescriptionStrategy;
import com.nkunku.designpatterns.strategy.impl.FemaleDescription;
import com.nkunku.designpatterns.strategy.impl.MaleDescription;
import com.nkunku.models.HumanBeing.Gender;

/**
 * Factory to get the right human description.
 *
 * @author Mike
 */
final class HumanDescriptionFactory {

	/**
	 * @param gender The gender.
	 * @return The right human description.
	 */
	static HumanDescriptionStrategy fromGender(final Gender gender) {
		switch (gender) {
			case FEMALE:
				return new FemaleDescription();
			case MALE:
				return new MaleDescription();
			default:
				throw new IllegalStateException("The gender '" + gender.name() + "' is not handled");
		}
	}
}
