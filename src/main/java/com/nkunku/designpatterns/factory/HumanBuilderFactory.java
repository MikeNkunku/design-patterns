package com.nkunku.designpatterns.factory;

import com.nkunku.models.HumanBeing.Gender;
import com.nkunku.models.impl.AbstractHuman;
import com.nkunku.models.impl.Female;
import com.nkunku.models.impl.Male;

/**
 * Factory to get the right human builder.
 *
 * @author Mike
 */
final class HumanBuilderFactory {

	/**
	 * @param gender The gender.
	 * @return The right human builder.
	 */
	static AbstractHuman.Builder<? extends AbstractHuman, ? extends AbstractHuman.Builder<?, ?>> fromGender(final Gender gender) {
		switch (gender) {
			case FEMALE:
				return new Female.Builder();
			case MALE:
				return new Male.Builder();
			default:
				throw new IllegalStateException("The gender '" + gender.name() + "' is not handled");
		}
	}
}
