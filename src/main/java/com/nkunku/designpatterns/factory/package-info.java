/**
 * Package dedicated to the mastering of the <b>Factory method</b> design pattern.
 */
package com.nkunku.designpatterns.factory;
