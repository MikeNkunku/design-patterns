package com.nkunku.designpatterns.builder;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.nkunku.models.HumanBeing.Gender;
import com.nkunku.models.Questions;
import com.nkunku.models.impl.AbstractHuman;
import com.nkunku.models.impl.Female.Builder;
import com.nkunku.models.impl.Male;

/**
 * Class to be run in order to test the <b>Builder</b> design pattern.
 *
 * @author Mike
 */
public class Demo {

	/**
	 * @param arguments Arguments received via command line.
	 */
	public static void main(final String[] arguments) {
		AbstractHuman.Builder<? extends AbstractHuman, ? extends AbstractHuman.Builder<?, ?>> builder = null;
		boolean validAnswer = false;
		System.out.println(Questions.GENDER.question());

		try (final Scanner scanner = new Scanner(System.in)) {
			while (!validAnswer) {
				try {
					builder = Gender.FEMALE.equals(Gender.from(scanner.nextLine())) ? new Builder() : new Male.Builder();
					validAnswer = true;
				} catch (final IllegalArgumentException e) {
					System.out.println(e.getMessage());
				}
			}

			final List<Questions> remainingQuestions = Arrays.stream(Questions.values()).filter(question -> !Questions.GENDER.equals(question)).collect(Collectors.toList());
			for (final Questions question : remainingQuestions) {
				handleQuestion(scanner, question, builder);
			}
		}

		System.out.format("%n%s", builder.build());
	}

	/**
	 * @param scanner The scanner to get the user answer.
	 * @param question The question to ask to the user.
	 * @param builder The builder to update.
	 */
	private static void handleQuestion(final Scanner scanner, final Questions question,
		final AbstractHuman.Builder<? extends AbstractHuman, ? extends AbstractHuman.Builder<?, ?>> builder) {
		System.out.println(question.question());
		question.builderUpdater().accept(builder, scanner.nextLine());
	}
}
