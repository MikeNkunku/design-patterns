/**
 * Package containing elements to master the <b>Builder</b> design pattern.
 */
package com.nkunku.designpatterns.builder;
